f n = sum[x | x <- [1..n], x `rem` 3 == 0 || x `rem` 5 == 0]
main = print (f 99)