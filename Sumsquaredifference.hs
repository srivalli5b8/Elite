sumofsquares n = sum [x * x | x <- [1..n]]
sumofnumbers n = sum [x | x <- [1..n]]
difference n = sumofnumbers n * sumofnumbers n - sumofsquares n
main = print (difference 100)