import Data.Char

factorialsum = sum . map (product . enumFromTo 1 . digitToInt) . show
digitfactorial = sum [x | x <- [3..1000000], x == factorialsum x]

main = print (digitfactorial) 