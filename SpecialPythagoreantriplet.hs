triplet n = [product [a, b, c]| a <- [1..n], b <- [a + 1..n], c <- [n - a - b], a ^ 2 + b ^ 2 == c ^ 2]

main = print(triplet 1000)