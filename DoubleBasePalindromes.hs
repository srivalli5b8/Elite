import Numeric
import Data.Char
 
toBinary n = showIntAtBase 2 intToDigit n ""

isPalindrome n = n == reverse n
 
doubleBasePalindromes = sum [x | x <- [1, 3..1000000], isPalindrome(show x), isPalindrome(toBinary x)]

main = do

print(doubleBasePalindromes)