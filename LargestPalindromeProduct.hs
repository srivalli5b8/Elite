largestPalindrome = maximum [n | x <- [100..999], y <- [x..999], let n = x * y, let m = show n, m == reverse m]
main = print(largestPalindrome)