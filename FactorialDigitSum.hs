import Data.Char

sumofDigits = sum (map digitToInt n)
    where n = show (product[1..100])

main = print (sumofDigits)