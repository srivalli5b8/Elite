fibsum n = sum [ x | x <- takeWhile (<= n) fib, even x]
  where
    fib = 1 : 1 : zipWith (+) fib (tail fib)
main = print (fibsum 4000000)