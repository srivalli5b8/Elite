isFactor n x = rem n x ==0
isqrt = floor . sqrt. fromIntegral
hasFactors n = or $ map (isFactor n)[2..isqrt n]
primes n = sum[x | x <- [2..n], not (hasFactors x)]

main = print(primes 2000000)