import Data.Char

sumofindividualdigits = sum (map digitToInt n)
    where n = show (2 ^ 1000)

main = print (sumofindividualdigits)