fib = 0 : 1 : zipWith (+) fib (tail fib)
fibindex =  length x 
   where x = takeWhile (< 10 ^ 999) fib

main = print (fibindex)
